
FROM golang:1.20.1-alpine

WORKDIR /app

# Download Go modules
COPY go.mod go.sum ./
RUN go mod download


COPY . ./

# Build
RUN CGO_ENABLED=0 GOOS=linux go build -o /data_logger

EXPOSE 8080

# Run
CMD ["/data_logger"]