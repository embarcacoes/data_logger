package routes

import (
	controller "data_logger/controllers"
	"fmt"
	"log"
	"net/http"

	"github.com/rs/cors"
)

func InitServer() {
	mux := http.NewServeMux()

	mux.HandleFunc("/logger/", controller.GetAllData) // Update this line of code
	mux.HandleFunc("/logger/filter", controller.GetData)
	//mux.HandleFunc("/delete", controller.DeleteData)
	mux.HandleFunc("/logger/count", controller.CountData)
	mux.HandleFunc("/logger/find/count", controller.FilterAndCountData)
	mux.HandleFunc("/logger/itens", controller.ListItens)

	// cors.Default() setup the middleware with default options being
	// all origins accepted with simple methods (GET, POST). See
	// documentation below for more options.
	handler := cors.Default().Handler(mux)
	fmt.Printf("Starting server at port 8080\n")
	if err := http.ListenAndServe("0.0.0.0:8080", handler); err != nil {
		log.Fatal(err)
	}
}
