package models

import (
	"encoding/json"
	"time"
)

type DataModel struct {
	Topic    string    `json:"topic"`
	Payload  string    `json:"payload"`
	Datetime time.Time `json:"datetime"`
}

func DataToJson(data DataModel) string {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return ""
	}
	return string(jsonData)
}

func JsonToData(jsonData string) DataModel {
	var data DataModel
	json.Unmarshal([]byte(jsonData), &data)
	return data
}

func NewDataModel(topic string, payload string, datetime time.Time) *DataModel {
	return &DataModel{Topic: topic, Payload: payload, Datetime: datetime}
}
