package mqtt

import (
	config "data_logger/configs"
	data "data_logger/models"
	"fmt"
	"os"
	"strconv"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var messagePubHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf("Received message: %s from topic: %s\n", msg.Payload(), msg.Topic())

	var datamodel = data.NewDataModel(msg.Topic(), string(msg.Payload()), time.Now())

	config.InsertData(datamodel, "data")
}

var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
	fmt.Println("Connected")

	client.Subscribe("#", 0, nil)
}

var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
	fmt.Printf("Connect lost: %v", err)
}

func InitMqttClient() {
	var broker = os.Getenv("MQTT_SERVER")
	port, _ := strconv.Atoi(os.Getenv("MQTT_PORT"))
	var user = os.Getenv("MQTT_USER")
	var password = os.Getenv("MQTT_PASS")
	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("mqtt://%s:%d", broker, port))
	opts.SetClientID("go_mqtt_client_ data_logger")
	opts.SetUsername(user)
	opts.SetPassword(password)
	opts.SetDefaultPublishHandler(messagePubHandler)
	opts.OnConnect = connectHandler
	opts.OnConnectionLost = connectLostHandler
	client := mqtt.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		fmt.Printf("Erro ao iniciar MQTT")
		panic(token.Error())
	}
}
